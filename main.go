package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/AloneLegion/rest_api_tmpl/controllers"
	"gitlab.com/AloneLegion/rest_api_tmpl/models"
)

func main() {
	route := gin.Default()

	// Подключение к базе данных
	models.ConnectDB()

	// Маршруты
	route.GET("/books", controllers.FindBooks)
	route.POST("/books", controllers.CreateBook)
	route.GET("/books/:id", controllers.FindBook)
	route.PATCH("/books/:id", controllers.UpdateBook)
	route.DELETE("/books/:id", controllers.DeleteBook)

	// Запуск сервера
	route.Run()
}
